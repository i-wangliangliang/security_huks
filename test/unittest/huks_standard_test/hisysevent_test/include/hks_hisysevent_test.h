/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HKS_HISYSEVENT_TEST_H
#define HKS_HISYSEVENT_TEST_H

namespace OHOS {
namespace Security {
namespace Huks {
int HksHiSysEventTest001(void);
int HksHiSysEventTest002(void);
int HksHiSysEventTest003(void);
int HksHiSysEventTest004(void);
int HksHiSysEventTest005(void);
int HksHiSysEventTest006(void);
int HksHiSysEventTest007(void);
int HksHiSysEventTest008(void);
int HksHiSysEventTest009(void);
int HksHiSysEventTest010(void);
int HksHiSysEventTest011(void);
}  // namespace Huks
}  // namespace Security
}  // namespace OHOS
#endif // HKS_HISYSEVENT_TEST_H
